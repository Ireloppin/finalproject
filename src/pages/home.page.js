import "./../components/app.content.component";

export class HomePage extends HTMLElement {
  connectedCallback() {
    this.innerHTML = `<app-content></app-content>`;
  }
}

customElements.define("home-page", HomePage);
