import "../components/recipe.component";

export class RecipePage extends HTMLElement {
  connectedCallback() {
    this.innerHTML = `
    <one-recipe></one-recipe>`;
  }
}

customElements.define("recipe-page", RecipePage);
