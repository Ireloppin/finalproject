import "../components/favorite.component";

export class FavoritePage extends HTMLElement {
  connectedCallback() {
    this.innerHTML = `
    <favorite-ui></favorite-ui>`;
  }
}

customElements.define("favorite-page", FavoritePage);
