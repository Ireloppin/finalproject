import "../components/edit.favorite.component";

export class EditPage extends HTMLElement {
  connectedCallback() {
    this.innerHTML = `
    <edit-favorite></edit-favorite>`;
  }
}

customElements.define("edit-page", EditPage);
