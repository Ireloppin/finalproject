import { LitElement, html } from "lit";

export class RecipeCardUI extends LitElement {
  static get properties() {
    return {
      recipe: { type: Object },
    };
  }

  render() {
    return html`
      <section class="recipe-card">
        <a href="/recipe/${this.recipe.id}">
          <figure>
            <img
              class="recipe-card--img"
              src="${this.recipe?.image}"
              alt="Image of ${this.recipe?.name}"
            />
          </figure>
          <p>
            <span class="recipe-card--text">${this.recipe?.name}</span>
            <span class="recipe-card--text"
              >Tradition: ${this.recipe?.tradition}
            </span>
          </p>
        </a>
        <button @click=${this.handleAdd} class="button button--add">
          Add to favorites
        </button>
      </section>
    `;
  }
  handleAdd(e) {
    e.preventDefault();
    const AddingFavorite = new CustomEvent("add-favorite", {
      bubbles: true,
      composed: true,
      detail: {
        id: `${this.recipe?.id}`,
      },
    });
    this.dispatchEvent(AddingFavorite);
  }

  createRenderRoot() {
    return this;
  }
}

customElements.define("recipe-card", RecipeCardUI);
