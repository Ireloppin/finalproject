import { LitElement, html } from "lit";

export class FavoriteCardUI extends LitElement {
  static get properties() {
    return {
      favorite: { type: Object },
    };
  }

  render() {
    return html`
      <article class="favorite-card" id="${this.favorite?.name}">
        <a href="/favorite/${this.favorite?.id}" class="favorite-card--info">
          <p class="favorite-card--p">
            <span class="favorite-card--text">${this.favorite?.name}</span>
          </p>
          <figure>
            <img
              class="favorite-card--img"
              src="${this.favorite?.image}"
              alt="Image of ${this.favorite?.name}"
            />
          </figure>
        </a>
        <section class="favorite-card--buttons">
          <button @click=${this.handleDelete} class="button" id="delete-button">
            Delete
          </button>

          <a href="/edit/${this.favorite.id}">
            <button class="button edit-button">Edit</button>
          </a>
        </section>
      </article>
    `;
  }
  createRenderRoot() {
    return this;
  }
  handleDelete(e) {
    e.preventDefault();
    const DeleteFavorite = new CustomEvent("delete-favorite", {
      bubbles: true,
      composed: true,
      detail: {
        id: `${this.favorite?.id}`,
      },
    });
    this.dispatchEvent(DeleteFavorite);
  }
}

customElements.define("favorite-card", FavoriteCardUI);
