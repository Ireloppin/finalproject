import { proxy } from "valtio/vanilla";

export const recipesState = proxy({ recipes: "" });
export const favoritesState = proxy({ favorites: "" });
