import "./main.css";
import "./components/favorites.content.component";
import "./pages/home.page";
import "./pages/recipe.page";
import "./pages/favorite.page";
import "./pages/edit.page";
import { Router } from "@vaadin/router";

const outlet = document.getElementById("outlet");
export const router = new Router(outlet);
router.setRoutes([
  {
    path: "/",
    component: "home-page",
  },
  {
    path: "/recipe/:id",
    component: "recipe-page",
  },
  {
    path: "/favorite/:id",
    component: "favorite-page",
  },
  {
    path: "/edit/:id",
    component: "edit-page",
  },
  { path: "(.*)", redirect: "/" },
]);
