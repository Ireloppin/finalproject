import { LitElement, html } from "lit";
import { MealDataUseCase } from "../usecases/meal-list.usecase";
import { FilterByCategoryUseCase } from "../usecases/filter-category.usecase";
import { FilterByTraditionUseCase } from "../usecases/filter-tradition.usecase";
import { FilterByIngredientUseCase } from "../usecases/filter-ingredient.usecase";
import "./recipes.content.component";
import { recipesState } from "../states/state";
import { snapshot } from "valtio/vanilla";

export class AppContent extends LitElement {
  static get properties() {
    return {
      recipes: { type: Array },
      recipesList: { type: Array },
      categoriesArray: { type: Array },
      traditionsArray: { type: Array },
      ingredientsArray: { type: Array },
      selectedCategory: { type: String },
      selectedTradition: { type: String },
      selectedIngedient: { type: String },
      filterRecipes: { type: Array },
      filterByCategory: { type: Array },
      filterByTradition: { type: Array },
      filterbyIngredient: { type: Array },
      showError: { type: Boolean },
    };
  }
  async connectedCallback() {
    super.connectedCallback();
    this.showError = false;
    recipesState.recipes = await MealDataUseCase.execute();
    this.recipesList = snapshot(recipesState.recipes);

    this.categoriesArray = [
      ...new Set(this.recipesList.map((recipe) => recipe.category).sort()),
    ];
    this.traditionsArray = [
      ...new Set(this.recipesList.map((recipe) => recipe.tradition).sort()),
    ];
    this.ingredientsArray = [
      ...new Set(
        this.recipesList
          .map((recipe) => recipe.ingredients)
          .flat()
          .sort()
      ),
    ];
    this.ingredientsArray.shift();

    document.addEventListener(
      "newSearch",
      (this.newSearch = (e) => {
        this.recipesList = snapshot(recipesState.recipes);
        this.showError = false;
      })
    );
    document.addEventListener(
      "filterCategory",
      (this.filterCategory = (e) => {
        this.selectedCategory = e.detail.category;
        this.filterByCategory = FilterByCategoryUseCase.execute(
          this.recipesList,
          this.selectedCategory
        );
        this.recipesList = this.filterByCategory;
        if (this.recipesList.length <= 0) {
          this.showError = true;
        }
      })
    );
    document.addEventListener(
      "filterTradition",
      (this.filterTradition = (e) => {
        this.selectedTradition = e.detail.tradition;
        this.filterByTradition = FilterByTraditionUseCase.execute(
          this.recipesList,
          this.selectedTradition
        );
        this.recipesList = this.filterByTradition;
        if (this.recipesList.length <= 0) {
          this.showError = true;
        }
      })
    );
    document.addEventListener(
      "filterIngredient",
      (this.filterTradition = (e) => {
        this.selectedIngredient = e.detail.ingredient;
        this.filterbyIngredient = FilterByIngredientUseCase.execute(
          this.recipesList,
          this.selectedIngredient
        );
        this.recipesList = this.filterbyIngredient;
        if (this.recipesList.length <= 0) {
          this.showError = true;
        }
      })
    );
  }
  render() {
    return html`
      <recipes-content
        .categoriesArray="${this.categoriesArray}"
        .traditionsArray="${this.traditionsArray}"
        .ingredientsArray="${this.ingredientsArray}"
        .recipesList=${this.recipesList}
        ?showError="${this.showError}"
        class="recipes-block"
      ></recipes-content>
    `;
  }
  disconnectedCallback() {
    super.disconnectedCallback();
    document.removeEventListener("filterCategory", this.filterCategory);
    document.removeEventListener("filterTradition", this.filterTradition);
    document.removeEventListener("filterIngredient", this.filterTradition);
    document.removeEventListener("newSearch", this.newSearch);
  }
  createRenderRoot() {
    return this;
  }
}

customElements.define("app-content", AppContent);
