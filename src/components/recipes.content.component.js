import { LitElement, html } from "lit";
import "./recipe.component";
import "../ui/recipe.card.ui";
import "./edit.favorite.component";
import "./favorite.component";
import "./search.form.component";

export class RecipesContent extends LitElement {
  static get properties() {
    return {
      recipesList: { type: Array },
      showAllRecipes: { type: Boolean },
      showOneRecipe: { type: Boolean },
      showError: { type: Boolean },
      categoriesArray: { type: Array },
      traditionsArray: { type: Array },
      ingredientsArray: { type: Array },
    };
  }

  render() {
    return html`<section class="all-recipes">
      <search-form
        class="search-block"
        .categoriesArray="${this.categoriesArray}"
        .traditionsArray="${this.traditionsArray}"
        .ingredientsArray="${this.ingredientsArray}"
      ></search-form>
      <body>
        ${this.showError
          ? html`
              <section class="error">
                <p class="error--text">
                  <span class="error--text__span"
                    >Sorry, we can´t found results for this search.</span
                  >
                </p>
                <p>For a new search click New Search</p>
              </section>
            `
          : html`
              <ul class="recipe-cards-content">
                ${this.recipesList?.map(
                  (recipe) => html`<li>
                    <recipe-card .recipe="${recipe}"></recipe-card>
                  </li> `
                )}
              </ul>
            `}
      </body>
    </section>`;
  }

  createRenderRoot() {
    return this;
  }
}

customElements.define("recipes-content", RecipesContent);
