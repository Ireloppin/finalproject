import { LitElement, html } from "lit";
import { RecipesDataUseCase } from "../usecases/recipes-storage.usecase";

export class RecipeUI extends LitElement {
  static get properties() {
    return {
      recipes: { type: Array },
      recipe: { type: Object },
      id: { type: String },
    };
  }

  async connectedCallback() {
    super.connectedCallback();
    this.recipes = await RecipesDataUseCase.execute();
    this.id = window.location.pathname.split("/")[2];
    this.recipe = this.recipes.filter((recipe) => recipe.id === this.id)[0];
  }

  render() {
    return html`
      <section>
        <article class="recipe-ui">
          <section class="buttons-section">
            <button @click=${this.handleAdd} class="button button--add">
        Add to favorites
      </button>
      <a href="/"><button  class="button close-button">
        Close
      </button></a>
          </section>
          <header class="recipe-ui--header">
          <section>
              <p class="recipe-card--title">${this.recipe?.name}</p>

              <p class="recipe-card--text"
                > <span>Origin:</span> ${this.recipe?.tradition}
              </p>
              <p class="recipe-card--text"
                > <span>Category</span>: ${this.recipe?.category}
              </p>
            </section>
            <figure>
              <img
                class="recipe-card--img"
                src="${this.recipe?.image}"
                alt="Image of ${this.recipe?.name}"
              />
            </figure>
                     </p>
          </header>
          
          <p class="recipe--text">
              <span>Instructions:</span> <textarea readonly class="recipe--text"
              >  ${this.recipe?.instructions}
            </textarea></p>
            <section class="recipe--ingredients"
              >
              <div class= "recipe--text">
              <span>Ingredients:</span> 
              <ul>
              ${this.recipe?.ingredients.map(
                (ingredient) => html`<li>${ingredient}</li>`
              )}</ul>
              </div>
               <div class= "recipe--text">
            <span>Measures:</span>  <ul>
              ${this.recipe?.measures.map(
                (measure) => html`<li>${measure}</li>`
              )}</ul></div>
            </section>
         
        </article>
      </section>
    `;
  }

  handleAdd(e) {
    e.preventDefault();
    window.location = "/";
    const AddingFavorite = new CustomEvent("add-favorite", {
      bubbles: true,
      composed: true,
      detail: {
        id: `${this.recipe?.id}`,
      },
    });
    this.dispatchEvent(AddingFavorite);
  }
  createRenderRoot() {
    return this;
  }
}

customElements.define("one-recipe", RecipeUI);
