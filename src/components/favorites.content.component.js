import { LitElement, html } from "lit";
import { AddFavUseCase } from "../usecases/add-fav.usecase";
import { DeleteFavUseCase } from "../usecases/delete-fav.usecase";
import "../ui/favorite.card.ui";
import { UpdateFavUseCase } from "../usecases/update-fav.usecase";
import { FavoritesDataUseCase } from "../usecases/favorites-data.usecase";
import { favoritesState, recipesState } from "../states/state";
import { snapshot } from "valtio/vanilla";

export class FavoritesContent extends LitElement {
  static get properties() {
    return {
      favorites: { type: Array },
      favoritesStorage: { type: Array },
      newFavorite: { type: Object },
      selectedId: { type: Number },
      forUpdateFav: { type: Object },
      noFavorites: { type: Boolean },
      location: { type: Object },
      id: { type: String },
    };
  }

  async connectedCallback() {
    super.connectedCallback();
    this.favorites = [];
    favoritesState.favorites = await FavoritesDataUseCase.execute();

    this.favoritesStorage = snapshot(favoritesState.favorites);
    if (this.favoritesStorage.length <= 0) {
      this.noFavorites = true;
    } else {
      this.noFavorites = false;
    }

    document.addEventListener(
      "add-favorite",
      (this.addFavorite = async (e) => {
        this.noFavorites = false;
        this.id = e.detail.id;
        this.recipes = snapshot(recipesState.recipes);
        this.newFavorite = this.recipes.filter(
          (recipe) => recipe.id === this.id
        )[0];
        this.favoritesStorage = await AddFavUseCase.execute(
          this.favoritesStorage,
          this.newFavorite
        );
        favoritesState.favorites = await FavoritesDataUseCase.execute();
      })
    );

    document.addEventListener(
      "delete-favorite",
      (this.deleteFavorite = async (e) => {
        this.favorites = this.favoritesStorage;
        this.selectedId = e.detail.id;
        this.favoritesStorage = await DeleteFavUseCase.execute(
          this.favorites,
          this.selectedId
        );
        if (this.favoritesStorage.length <= 0) {
          this.noFavorites = true;
        } else {
          this.noFavorites = false;
        }
        favoritesState.favorites = await FavoritesDataUseCase.execute();
      })
    );
    document.addEventListener(
      "updated-favorite",
      (this.updatedFavorite = async (e) => {
        window.location = "/";
        this.forUpdateFav = e.detail.updateFavorite;
        this.favoritesStorage = await UpdateFavUseCase.execute(
          this.favoritesStorage,
          this.forUpdateFav
        );
        favoritesState.favorites = await FavoritesDataUseCase.execute();
      })
    );
  }

  render() {
    return html`
      <section class="favorite-content" id="favorite-content">
        <h2 class="favorites--h2">My Favorites</h2>
        <body>
          ${this.noFavorites
            ? html`<p class="favorites--no-favorites">
                No recipes saved in favorites
              </p>`
            : html`<ul class="favorite-cards-content content">
                ${this.favoritesStorage?.map(
                  (favorite) =>
                    html`<li>
                      <favorite-card .favorite="${favorite}"></favorite-card>
                    </li> `
                )}
              </ul>`}
        </body>
      </section>
    `;
  }

  disconnectedCallback() {
    super.disconnectedCallback();
    document.removeEventListener("add-favorite", this.addFavorite);
    document.removeEventListener("delete-favorite", this.deleteFavorite);
    document.removeEventListener("updated-favorite", this.updatedFavorite);
  }
  createRenderRoot() {
    return this;
  }
}

customElements.define("favorites-content", FavoritesContent);
