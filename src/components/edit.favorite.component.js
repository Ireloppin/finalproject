import { LitElement, html } from "lit";
import { favoritesState } from "../states/state";
import { snapshot } from "valtio/vanilla";
import { Recipe } from "../model/recipe";

export class EditForm extends LitElement {
  static get properties() {
    return {
      id: { type: String },
      favorite: { type: Object },
      recipes: { type: Object },
      updatedFav: { type: Object },
      name: { type: String },
      tradition: { type: String },
      category: { type: String },
      instructions: { type: String },
      ingredients: { type: Array },
      measures: { type: Array },
    };
  }

  connectedCallback() {
    super.connectedCallback();
    this.recipes = snapshot(favoritesState.favorites);
    this.id = window.location.pathname.split("/")[2];
    this.favorite = this.recipes.filter((recipe) => recipe.id === this.id)[0];

    this.name = this.favorite.name;
    this.tradition = this.favorite.tradition;
    this.category = this.favorite.category;
    this.instructions = this.favorite.instructions;
    this.ingredients = this.favorite.ingredients;
    this.measures = this.favorite.measures;
  }

  render() {
    return html` <article class="recipe-ui">
      <section class="buttons-section">
        <button @click=${this.handleUpdate} class="button" id="update-button">
          Save Changes
        </button>
        <a href="/"><button class="button" id="close-button">Close</button></a>
      </section>
      <header class="recipe-ui--header">
        <p>
          <label class="recipe-card--text" for="name">Name</label>
          <input
            class="form--input"
            type="text"
            id="name-input"
            .value="${this.name}"
          />

          <label class="recipe-card--text" for="tradition">Tradition</label>
          <input
            class="form--input"
            type="text"
            id="tradition-input"
            .value="${this.tradition}"
          />

          <label class="recipe-card--text" for="category">Category</label>
          <input
            class="form--input"
            type="text"
            id="category-input"
            .value="${this.category}"
          />
        </p>
        <figure>
          <img
            class="recipe-card--img"
            src="${this.favorite?.image}"
            alt="Image of ${this.favorite?.name}"
          />
        </figure>
      </header>
      <section class="body-recipe-ui">
        <p>
          <label class="recipe--text recipe-card--text" for="instructions"
            >Instructions</label
          >
          <textarea
            class="form--textarea recipe--text"
            id="instructions-input"
            .value="${this.instructions}"
          ></textarea>
        </p>
        <section class="recipe--ingredients">
          <div class="recipe--text">
            <span>Ingredients:</span>
            <ul>
              ${this.favorite?.ingredients.map(
                (ingredient) => html`<li>${ingredient}</li>`
              )}
            </ul>
          </div>
          <div class="recipe--text">
            <span>Measures:</span>
            <ul>
              ${this.favorite?.measures.map(
                (measure) => html`<li>${measure}</li>`
              )}
            </ul>
          </div>
        </section>
      </section>
    </article>`;
  }
  handleUpdate(e) {
    e.preventDefault();
    const UpdatedFavorite = new CustomEvent("updated-favorite", {
      bubbles: true,
      composed: true,
      detail: {
        updateFavorite: new Recipe({
          id: `${this.favorite?.id}`,
          name: document.querySelector("#name-input").value,
          category: document.querySelector("#category-input").value,
          instructions: document.querySelector("#instructions-input").value,
          tradition: document.querySelector("#tradition-input").value,
          image: `${this.favorite?.image}`,
          ingredients: this.ingredients,
          measures: this.measures,
        }),
      },
    });
    this.dispatchEvent(UpdatedFavorite);
  }

  createRenderRoot() {
    return this;
  }
}
customElements.define("edit-favorite", EditForm);
