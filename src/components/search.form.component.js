import { LitElement, html } from "lit";

export class SearchForm extends LitElement {
  static get properties() {
    return {
      categoriesArray: { type: Array },
      traditionsArray: { type: Array },
      ingredientsArray: { type: Array },
      categoryDisabled: { type: Boolean },
      traditionDisabled: { type: Boolean },
      ingredientDisabled: { type: Boolean },
    };
  }

  connectedCallback() {
    super.connectedCallback();
    this.disabled = false;
    document.addEventListener(
      "filterCategory",
      (this.filterCategory = () => {
        this.categoryDisabled = true;
      })
    );
    document.addEventListener(
      "filterTradition",
      (this.filterTradition = () => {
        this.traditionDisabled = true;
      })
    );
    document.addEventListener(
      "filterIngredient",
      (this.filterIngredient = () => {
        this.ingredientDisabled = true;
      })
    );

    document.addEventListener(
      "newSearch",
      (this.newSearch = () => {
        document.querySelector("#category");
        category.value = "";
        document.querySelector("#tradition");
        tradition.value = "";
        document.querySelector("#ingredients");
        ingredient.value = "";
        this.categoryDisabled = false;
        this.traditionDisabled = false;
        this.ingredientDisabled = false;
      })
    );
  }

  render() {
    return html`<section class="section-content search--content">
    <form class="search--form">
      <fieldset>
        <label for="category" hidden> Category</label>
        <select
          @change=${this.handleCategory}
          ?disabled="${this.categoryDisabled}"
          name="select"
          id="category"
          class= "search--form__select"
        >
          <option value="" selected disabled hidden>Category</option>
          ${this.categoriesArray?.map(
            (category) => html`<option .value="${category}">
              ${category}
            </option>`
          )}
        </select>
      </fieldset>
      <fieldset>
        <label for="tradition" hidden>Tradition</label>
        <select
          @change=${this.handleTradition}
          ?disabled="${this.traditionDisabled}"
          name="select"
          id="tradition"
          class= "search--form__select"
        >
          <option value="" selected disabled hidden>Tradition</option>
          ${this.traditionsArray?.map(
            (tradition) => html`<option .value="${tradition}">
              ${tradition}
            </option>`
          )}
        </select>
      </fieldset>
      <fieldset>
        <label for="ingredient" hidden>Ingredient</label>
        <select
          @change=${this.handleIngredient}
          ?disabled="${this.ingredientDisabled}"
          name="select"
          id="ingredient"
          class= "search--form__select"
        >
          <option value="" selected disabled hidden>Ingredient</option>
          ${this.ingredientsArray?.map(
            (ingredient) => html`<option
              class="form--ingredient-option"
              .value="${ingredient}"
            >
              ${ingredient}
            </option>`
          )}
        </select>
      </fieldset>

         <button
                  @click=${this.handleNewSearch}
                  class="button search--button"
                >
                  New Search
          </button>

    </form>
  </section>    
        
      </section>
    `;
  }

  handleNewSearch(e) {
    e.preventDefault();
    const newSearch = new CustomEvent("newSearch", {
      bubbles: true,
      composed: true,
    });
    this.dispatchEvent(newSearch);
  }

  handleCategory(e) {
    e.preventDefault();
    const filterCategory = new CustomEvent("filterCategory", {
      bubbles: true,
      composed: true,
      detail: {
        category: e.target.value,
      },
    });
    this.dispatchEvent(filterCategory);
  }
  handleTradition(e) {
    e.preventDefault();
    const filterTradition = new CustomEvent("filterTradition", {
      bubbles: true,
      composed: true,
      detail: {
        tradition: e.target.value,
      },
    });
    this.dispatchEvent(filterTradition);
  }
  handleIngredient(e) {
    e.preventDefault();
    const filterIngredient = new CustomEvent("filterIngredient", {
      bubbles: true,
      composed: true,
      detail: {
        ingredient: e.target.value,
      },
    });
    this.dispatchEvent(filterIngredient);
  }

  disconnectedCallback() {
    super.disconnectedCallback();
    document.removeEventListener("newSearch", this.newSearch);
    document.removeEventListener("filterCategory", this.filterCategory);
    document.removeEventListener("filterTradition", this.filterTradition);
    document.removeEventListener("filterIngredient", this.filterIngredient);
  }
  createRenderRoot() {
    return this;
  }
}

customElements.define("search-form", SearchForm);
