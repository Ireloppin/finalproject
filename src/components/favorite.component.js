import { LitElement, html } from "lit";
import { favoritesState } from "../states/state";
import { snapshot } from "valtio/vanilla";

export class FavoriteUI extends LitElement {
  static get properties() {
    return {
      favorite: { type: Object },
      recipe: { type: Object },
      id: { type: String },
    };
  }

  connectedCallback() {
    super.connectedCallback();
    this.recipes = snapshot(favoritesState.favorites);
    this.id = window.location.pathname.split("/")[2];
    this.favorite = this.recipes.filter((recipe) => recipe.id === this.id)[0];
  }

  render() {
    return html`
      <article class="recipe-ui">
        <section class="buttons-section">
          <button @click=${this.handleDelete} class="button" id="delete-button">
            Delete
          </button>

          <a href="/edit/${this.favorite.id}">
            <button class="button" id="edit-button">Edit</button>
          </a>
          <a href="/"
            ><button class="button" id="close-button">Close</button></a
          >
        </section>
        <header class="recipe-ui--header">
          <section>
            <p class="recipe-card--title">${this.favorite?.name}</p>

            <p class="recipe-card--text">
              <span>Tradition:</span> ${this.favorite?.tradition}
            </p>
            <p class="recipe-card--text">
              <span>Category:</span> ${this.favorite?.category}
            </p>
          </section>
          <figure>
            <img
              class="recipe-card--img"
              src="${this.favorite?.image}"
              alt="Image of ${this.favorite?.name}"
            />
          </figure>
        </header>

        <p class="recipe--text">
          <span>Instructions:</span>
          <textarea readonly class="recipe--text">
 ${this.favorite?.instructions}</textarea
          >
        </p>
        <section class="recipe--ingredients">
          <div class="recipe--text">
            <span>Ingredients:</span>
            <ul>
              ${this.favorite?.ingredients.map(
                (ingredient) => html`<li>${ingredient}</li>`
              )}
            </ul>
          </div>
          <div class="recipe--text">
            <span>Measures:</span>
            <ul>
              ${this.favorite?.measures.map(
                (measure) => html`<li>${measure}</li>`
              )}
            </ul>
          </div>
        </section>
      </article>
    `;
  }
  handleDelete(e) {
    e.preventDefault();
    window.location = "/";
    const DeleteFavorite = new CustomEvent("delete-favorite", {
      bubbles: true,
      composed: true,
      detail: {
        id: `${this.favorite?.id}`,
      },
    });
    this.dispatchEvent(DeleteFavorite);
  }
  createRenderRoot() {
    return this;
  }
}

customElements.define("favorite-ui", FavoriteUI);
