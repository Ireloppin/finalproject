export class Recipe {
  constructor({
    id,
    name,
    category,
    tradition,
    instructions,
    image,
    ingredients,
    measures,
  }) {
    this.id = id;
    this.name = name;
    this.category = category;
    this.tradition = tradition;
    this.instructions = instructions;
    this.image = image;
    this.ingredients = ingredients;
    this.measures = measures;
  }
}
