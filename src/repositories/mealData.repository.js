import axios from "axios";

export class MealRepository {
  async getMealData() {
    return await (
      await axios.get("https://www.themealdb.com/api/json/v1/1/search.php?s")
    ).data;
  }
}
