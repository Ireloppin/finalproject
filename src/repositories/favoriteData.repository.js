export class FavoritesRepository {
  async getFavoritesData() {
    return await JSON.parse(localStorage.getItem("favorites") || "[]");
  }

  async updateLocalStorage(myFavsListUpdated) {
    localStorage.setItem("favorites", JSON.stringify(myFavsListUpdated));
    return JSON.parse(localStorage.getItem("favorites"));
  }
  async updateMealsStorage(recipes) {
    localStorage.setItem("recipes", JSON.stringify(recipes));
    return JSON.parse(localStorage.getItem("recipes"));
  }

  async getRecipesData() {
    return await JSON.parse(localStorage.getItem("recipes"));
  }
}
