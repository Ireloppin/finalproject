export class FilterByIngredientUseCase {
  static execute(recipes, selectedIngredient) {
    const filteredRecipes = recipes.filter((recipe) =>
      recipe.ingredients.includes(selectedIngredient)
    );
    return filteredRecipes;
  }
}
