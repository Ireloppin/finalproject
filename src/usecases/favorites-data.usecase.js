import { FavoritesRepository } from "../repositories/favoriteData.repository";
import { Recipe } from "../model/recipe";

export class FavoritesDataUseCase {
  static async execute() {
    let formattedFavorites;
    const repository = new FavoritesRepository();
    const favoritesData = await repository.getFavoritesData();
    if (favoritesData.length >= 0) {
      formattedFavorites = favoritesData.map((recipe) => {
        return new Recipe({
          id: recipe.id,
          name: recipe.name,
          category: recipe.category,
          tradition: recipe.tradition,
          instructions: recipe.instructions,
          image: recipe.image,
          ingredients: recipe.ingredients,
          measures: recipe.measures,
        });
      });
    }

    return formattedFavorites;
  }
}
