import { FavoritesRepository } from "../repositories/favoriteData.repository";
import { Recipe } from "../model/recipe";

export class RecipesDataUseCase {
  static async execute() {
    let formattedRecipes;
    const repository = new FavoritesRepository();
    const recipesData = await repository.getRecipesData();
    if (recipesData.length >= 0) {
      formattedRecipes = recipesData.map((recipe) => {
        return new Recipe({
          id: recipe.id,
          name: recipe.name,
          category: recipe.category,
          tradition: recipe.tradition,
          instructions: recipe.instructions,
          image: recipe.image,
          ingredients: recipe.ingredients,
          measures: recipe.measures,
        });
      });
    }

    return formattedRecipes;
  }
}
