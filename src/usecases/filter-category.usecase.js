export class FilterByCategoryUseCase {
  static execute(recipes, selectedCategory) {
    const filteredRecipes = recipes.filter(
      (recipe) => recipe.category === selectedCategory
    );
    return filteredRecipes;
  }
}
