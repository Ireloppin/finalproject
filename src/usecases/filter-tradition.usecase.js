export class FilterByTraditionUseCase {
  static execute(recipes, selectedTradition) {
    const filteredRecipes = recipes.filter(
      (recipe) => recipe.tradition === selectedTradition
    );
    return filteredRecipes;
  }
}
