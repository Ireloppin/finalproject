import { FavoritesRepository } from "../repositories/favoriteData.repository";
import { Recipe } from "../model/recipe";

export class AddFavUseCase {
  static async execute(myFavs, newFav) {
    let formattedFavorites;
    let myFavsListUpdated;
    if (myFavs.length <= 0) {
      myFavsListUpdated = [newFav, ...myFavs];
    } else {
      const isFavorite = myFavs.some((fav) => fav.id === newFav.id);

      if (isFavorite) {
        myFavsListUpdated = myFavs;
      } else {
        myFavsListUpdated = [newFav, ...myFavs];
      }
    }
    const repository = new FavoritesRepository();
    myFavsListUpdated = await repository.updateLocalStorage(myFavsListUpdated);
    if (myFavsListUpdated.length >= 0) {
      formattedFavorites = myFavsListUpdated.map((recipe) => {
        return new Recipe({
          id: recipe.id,
          name: recipe.name,
          category: recipe.category,
          tradition: recipe.tradition,
          instructions: recipe.instructions,
          image: recipe.image,
          ingredients: recipe.ingredients,
          measures: recipe.measures,
        });
      });
    }

    return formattedFavorites;
  }
}
