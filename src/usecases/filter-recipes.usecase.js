export class FilteredRecipesUseCase {
  static execute(
    recipes,
    selectedCategory,
    selectedTradition,
    selectedIngredient
  ) {
    const filteredRecipes = recipes.filter(
      (recipe) =>
        recipe.category === selectedCategory &&
        recipe.tradition === selectedTradition &&
        recipe.ingredients.includes(selectedIngredient)
    );
    return filteredRecipes;
  }
}
