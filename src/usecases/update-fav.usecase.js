import { Recipe } from "../model/recipe";
import { FavoritesRepository } from "../repositories/favoriteData.repository";

export class UpdateFavUseCase {
  static async execute(myFavs, forUpdateFav) {
    let myFavsListUpdated = myFavs.map((fav) =>
      fav.id === forUpdateFav.id ? (fav = forUpdateFav) : fav
    );
    const repository = new FavoritesRepository();
    myFavsListUpdated = await repository.updateLocalStorage(myFavsListUpdated);
    const formattedFavorites = myFavsListUpdated.map((recipe) => {
      return new Recipe({
        id: recipe.id,
        name: recipe.name,
        category: recipe.category,
        tradition: recipe.tradition,
        instructions: recipe.instructions,
        image: recipe.image,
        ingredients: recipe.ingredients,
        measures: recipe.measures,
      });
    });
    return formattedFavorites;
  }
}
