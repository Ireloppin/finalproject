import { FavoritesRepository } from "../repositories/favoriteData.repository";
import { Recipe } from "../model/recipe";

export class DeleteFavUseCase {
  static async execute(myFavs, selectedId) {
    let formattedFavorites;
    let myFavsListUpdated = myFavs.filter((fav) => fav.id !== selectedId);
    const repository = new FavoritesRepository();
    myFavsListUpdated = await repository.updateLocalStorage(myFavsListUpdated);
    if (myFavsListUpdated.length >= 0) {
      formattedFavorites = myFavsListUpdated.map((recipe) => {
        return new Recipe({
          id: recipe.id,
          name: recipe.name,
          category: recipe.category,
          tradition: recipe.tradition,
          instructions: recipe.instructions,
          image: recipe.image,
          ingredients: recipe.ingredients,
          measures: recipe.measures,
        });
      });
    }
    return formattedFavorites;
  }
}
