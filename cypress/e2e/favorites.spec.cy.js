/// <reference types="Cypress" />

describe("template spec", () => {
  it("passes", () => {
    cy.visit("http://localhost:8080/");
    cy.get(":nth-child(1) > recipe-card > .recipe-card > .button").click();
    cy.get(":nth-child(15) > recipe-card > .recipe-card > .button").click();
    cy.get(":nth-child(3) > recipe-card > .recipe-card > .button").click();

    cy.get("#Sushi > .favorite-card--info").click();
    cy.get(".recipe-card--title").contains("Sushi");

    cy.get(".buttons-section >  #delete-button").click();
    cy.get("#close-button").click();
    cy.get(":nth-child(1) > recipe-card > .recipe-card >  .button").click();

    cy.get(":nth-child(3) > recipe-card > .recipe-card > .button").click();
    cy.get("#Sushi > .favorite-card--info").click();

    cy.get(".recipe-ui").contains("Instructions: STEP 1");
    cy.get(" #edit-button").click();
    cy.get("#name-input").clear();
    cy.get("#name-input").type("My personal sushi");
    cy.get("#update-button").click();
    cy.get("#close-button").click();
    cy.get(":nth-child(5) > recipe-card > .recipe-card > .button").click();
    cy.get("#Corba > .favorite-card--buttons > #delete-button").click();
  });
});
