/// <reference types="Cypress" />

describe("template spec", () => {
  it("passes", () => {
    cy.visit("http://localhost:8080/");
    cy.get(".search--form > .button").contains("New Search");
    cy.get(":nth-child(1) > recipe-card > .recipe-card").contains("Corba");
    cy.get("#category").select("Seafood");
    cy.get("#tradition").select("Japanese");
    cy.get("#ingredient").select("Sushi Rice");
    cy.get(":nth-child(1) > recipe-card > .recipe-card").contains("Sushi");
    cy.get(".search--form > .button").click();
    cy.get(":nth-child(1) > recipe-card > .recipe-card").contains("Corba");
  });
});
