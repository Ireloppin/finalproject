import { MealRepository } from "../src/repositories/mealData.repository";
import { MealData } from "./fixtures/mealData";
import { MealDataUseCase } from "./../src/usecases/meal-list.usecase";

jest.mock("../src/repositories/mealData.repository");

describe("get a meal data", () => {
  beforeEach(() => {
    MealRepository.mockClear();
  });
  it("get meat data", async () => {
    MealRepository.mockImplementation(() => {
      return {
        getMealData: () => {
          return MealData;
        },
      };
    });
    const mealData = await MealDataUseCase.execute();

    expect(mealData.length).toBe(25);
    expect(mealData[0].name).toBe("Corba");
  });
});
