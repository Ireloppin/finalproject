import { Recipe } from "../src/model/recipe";
import { MealData } from "./fixtures/mealData";
import { FilterByTraditionUseCase } from "../src/usecases/filter-tradition.usecase";

describe("filter recipes by tradition", () => {
  it("only selected tradition", () => {
    const recipes = MealData.meals.map(
      (meal) =>
        new Recipe({
          id: meal.idMeal,
          name: meal.strMeal,
          category: meal.strCategory,
          tradition: meal.strArea,
          instructions: meal.strInstructions,
          image: meal.strMealThumb,
          ingredients: [
            meal.strIngredient1,
            meal.strIngredient2,
            meal.strIngredient3,
            meal.strIngredient4,
            meal.strIngredient5,
          ],
          measures: [
            meal.strMeasure1,
            meal.strMeasure2,
            meal.strMeasure3,
            meal.strMeasure4,
          ],
        })
    );

    const selectedTradition = "Egyptian";
    const recipesByTradition = FilterByTraditionUseCase.execute(
      recipes,
      selectedTradition
    );
    for (let i; i < recipesByTradition.length; i++) {
      expect(recipesByTradition[i].tradition).toBe(selectedTradition);
    }
  });
});
