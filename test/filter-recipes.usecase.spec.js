import { Recipe } from "../src/model/recipe";
import { MealData } from "./fixtures/mealData";
import { FilteredRecipesUseCase } from "../src/usecases/filter-recipes.usecase";

describe("filter recipes", () => {
  it("filter by category, origin or ingredient", () => {
    const recipes = MealData.meals.map(
      (meal) =>
        new Recipe({
          id: meal.idMeal,
          name: meal.strMeal,
          category: meal.strCategory,
          tradition: meal.strArea,
          instructions: meal.strInstructions,
          image: meal.strMealThumb,
          ingredients: [
            meal.strIngredient1,
            meal.strIngredient2,
            meal.strIngredient3,
            meal.strIngredient4,
            meal.strIngredient5,
          ],
          measures: [
            meal.strMeasure1,
            meal.strMeasure2,
            meal.strMeasure3,
            meal.strMeasure4,
          ],
        })
    );

    const selectedCategory = "Seafood";
    const selectedTradition = "Egyptian";
    const selectedIngredient = "Garlic Clove";
    const filteredRecipes = FilteredRecipesUseCase.execute(
      recipes,
      selectedCategory,
      selectedTradition,
      selectedIngredient
    );
    for (let i; i < filteredRecipes.length; i++) {
      expect(filteredRecipes[i].category).toBe(selectedCategory);
      expect(filteredRecipes[i].tradition).toBe(selectedTradition);
      expect(filteredRecipes[i].ingredients).toContain(selectedIngredient);
    }
  });
});
