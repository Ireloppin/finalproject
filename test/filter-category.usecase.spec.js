import { Recipe } from "../src/model/recipe";
import { MealData } from "./fixtures/mealData";
import { FilterByCategoryUseCase } from "../src/usecases/filter-category.usecase";

describe("filter recipes by category", () => {
  it("only selected category", () => {
    const recipes = MealData.meals.map(
      (meal) =>
        new Recipe({
          id: meal.idMeal,
          name: meal.strMeal,
          category: meal.strCategory,
          tradition: meal.strArea,
          instructions: meal.strInstructions,
          image: meal.strMealThumb,
          ingredients: [
            meal.strIngredient1,
            meal.strIngredient2,
            meal.strIngredient3,
            meal.strIngredient4,
            meal.strIngredient5,
          ],
          measures: [
            meal.strMeasure1,
            meal.strMeasure2,
            meal.strMeasure3,
            meal.strMeasure4,
          ],
        })
    );

    const selectedCategory = "Seafood";
    const recipesByCategory = FilterByCategoryUseCase.execute(
      recipes,
      selectedCategory
    );

    for (let i; i < recipesByCategory.length; i++) {
      expect(recipesByCategory[i].category).toBe(selectedCategory);
    }
  });
});
