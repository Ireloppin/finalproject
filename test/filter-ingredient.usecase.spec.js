import { Recipe } from "../src/model/recipe";
import { MealData } from "./fixtures/mealData";
import { FilterByIngredientUseCase } from "../src/usecases/filter-ingredient.usecase";

describe("filter recipes by category", () => {
  it("only selected category", () => {
    const recipes = MealData.meals.map(
      (meal) =>
        new Recipe({
          id: meal.idMeal,
          name: meal.strMeal,
          category: meal.strCategory,
          tradition: meal.strArea,
          instructions: meal.strInstructions,
          image: meal.strMealThumb,
          ingredients: [
            meal.strIngredient1,
            meal.strIngredient2,
            meal.strIngredient3,
            meal.strIngredient4,
            meal.strIngredient5,
          ],
          measures: [
            meal.strMeasure1,
            meal.strMeasure2,
            meal.strMeasure3,
            meal.strMeasure4,
          ],
        })
    );

    const selectedIngredient = "Garlic Clove";
    const recipesByIngredient = FilterByIngredientUseCase.execute(
      recipes,
      selectedIngredient
    );

    for (let i; i < recipesByIngredient.length; i++) {
      expect(recipesByCategory[i].ingredients).toContain(selectedIngredient);
    }
  });
});
