import { UpdateFavUseCase } from "../src/usecases/update-fav.usecase";
import { Recipe } from "../src/model/recipe";

describe("Update a recipe from my Favorites", () => {
  it("Update a recipe", async () => {
    const myFavsList = [
      {
        id: "52977",
        name: "Edited",
        category: "Side",
        instructions:
          "Pick through your lentils for any foreign debris, rinse them 2 or 3 times, drain, and set aside.  Fair warning, this will probably turn your lentils into a solid block that you’ll have to break up later\nIn a large pot over medium-high heat, sauté the olive oil and the onion with a pinch of salt for about 3 minutes, then add the carrots and cook for another 3 minutes.\nAdd the tomato paste and stir it around for around 1 minute. Now add the cumin, paprika, mint, thyme, black pepper, and red pepper as quickly as you can and stir for 10 seconds to bloom the spices. Congratulate yourself on how amazing your house now smells.\nImmediately add the lentils, water, broth, and salt. Bring the soup to a (gentle) boil.\nAfter it has come to a boil, reduce heat to medium-low, cover the pot halfway, and cook for 15-20 minutes or until the lentils have fallen apart and the carrots are completely cooked.\nAfter the soup has cooked and the lentils are tender, blend the soup either in a blender or simply use a hand blender to reach the consistency you desire. Taste for seasoning and add more salt if necessary.\nServe with crushed-up crackers, torn up bread, or something else to add some extra thickness.  You could also use a traditional thickener (like cornstarch or flour), but I prefer to add crackers for some texture and saltiness.  Makes great leftovers, stays good in the fridge for about a week.",
        tradition: "Turkish",
        image:
          "https://www.themealdb.com/images/media/meals/58oia61564916529.jpg",
        ingredients:
          "Lentils,Onion,Carrots,Tomato Puree,Cumin,Paprika,Mint,Thyme,Black Pepper,Red Pepper Flakes,Vegetable Stock,Water,Sea Salt,,,,,,,",
        measures:
          "1 cup ,1 large,1 large,1 tbs,2 tsp,1 tsp ,1/2 tsp,1/2 tsp,1/4 tsp,1/4 tsp,4 cups ,1 cup ,Pinch, , , , , , , ",
      },
      {
        id: "53060",
        name: "Burek",
        category: "Side",
        tradition: "Croatian",
        instructions:
          "Fry the finely chopped onions and minced meat in oil. Add the salt and pepper. Grease a round baking tray and put a layer of pastry in it. Cover with a thin layer of filling and cover this with another layer of filo pastry which must be well coated in oil. Put another layer of filling and cover with pastry. When you have five or six layers, cover with filo pastry, bake at 200ºC/392ºF for half an hour and cut in quarters and serve.",
        image:
          "https://www.themealdb.com/images/media/meals/tkxquw1628771028.jpg",
        ingredients:
          "Filo Pastry,Minced Beef,Onion,Oil,Salt,Pepper,,,,,,,,,,,,,,",
        measures:
          "1 Packet,150g,150g,40g,Dash,Dash, , , , , , , , , , , , , , ",
      },
    ];

    const forUpdateFav = new Recipe({
      id: "53060",
      name: "New",
      category: "Side",
      tradition: "Croatian",
      instructions:
        "Fry the finely chopped onions and minced meat in oil. Add the salt and pepper. Grease a round baking tray and put a layer of pastry in it. Cover with a thin layer of filling and cover this with another layer of filo pastry which must be well coated in oil. Put another layer of filling and cover with pastry. When you have five or six layers, cover with filo pastry, bake at 200ºC/392ºF for half an hour and cut in quarters and serve.",
      image:
        "https://www.themealdb.com/images/media/meals/tkxquw1628771028.jpg",
      ingredients:
        "Filo Pastry,Minced Beef,Onion,Oil,Salt,Pepper,,,,,,,,,,,,,,",
      measures: "1 Packet,150g,150g,40g,Dash,Dash, , , , , , , , , , , , , , ",
    });

    const myFavsListUpdated = await UpdateFavUseCase.execute(
      myFavsList,
      forUpdateFav
    );

    expect(myFavsListUpdated.length).toBe(2);
    expect(myFavsListUpdated[1].name).toBe("New");
  });
});
